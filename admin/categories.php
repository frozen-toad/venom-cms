<?php
include("includes/admin_header.php");
include("includes/admin_navbar.php");
?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Categories | Admin Area
                            <small>Author</small>
                        </h1>

                        <div class="col-xs-6">

                          <?php insert_categories(); ?>

                          <!-- ADD CATEGORY FORM -->
                          <form action="" method="post">

                            <div class="form-group">
                              <label for="add-category">Add Category</label>
                            <input type="text" name="cat_title" class="form-control">
                           </div>

                           <div class="form-group">
                           <input class="btn btn-primary" type="submit" name="submit" value="Add Category">
                          </div>

                          </form>

                          <?php
                          // UPDATE & EDIT CATEGORIES
                            if(isset($_GET['edit'])) {

                              $cat_id = $_GET['edit'];

                              include("includes/update_categories.php");

                            }
                          ?>

                        </div>

                        <div class="col-xs-6">

                          <table class="table table-bordered table-hover">
                            <thead>
                              <tr>
                                <th>Id</th>
                                <th>Category Title</th>
                              </tr>
                            </thead>

                            <tbody>
                              <tr>
                                <?php findAllCategories(); ?>

                                <?php deleteCategories(); ?>
                              </tr>
                            </tbody>

                          </table>

                        </div>

                    </div>
                </div><!-- /.row -->

            </div><!-- /.container-fluid -->

        </div><!-- /#page-wrapper -->

<?php include("includes/admin_footer.php"); ?>
