<!-- Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>&copy; 2022-<script>document.write(new Date().getFullYear())</script> FrozenToad. All Rights Reserved.</p>
            <p style="font-size:75%;">Site created with <a href="https://venom.frozentoad.net" target="_blank">Venom CMS</a>, an Open Source Content Management Project.</p>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>
