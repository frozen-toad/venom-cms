# How to contribute

I'm really glad you're reading this, because we need volunteer developers to help this project come to fruition.

If you haven't already, come find us in IRC ([#opengovernment](irc://chat.freenode.net/opengovernment) on freenode). We want you working on things you're excited about.

Here are some important resources:

* [Project's Main Site](https://frozentoad.net) see this project in a live environment,
* [Our roadmap](../..milestones) is the view of where we're heading,
* Bugs? [Issuse](../../issues) is where to report them
* IRC: chat.freenode.net channel [#frozentoad](irc://chat.freenode.net/frozentoad). We're usually there on weekends.

## Testing

We have a handful of  features, but most of our testbed still needs development.

## Submitting changes

Please send a [GitLab Pull Request to Venom CMS](https://gitlab.com/frozen-toad/venom-cms/pull/new/master) with a clear list of what you've done. We can always use more test coverage. Please follow our coding conventions (below) and make sure all of your commits are atomic (one feature per commit).

Always write a clear log message for your commits. One-line messages are fine for small changes, but bigger changes should look like this:

    $ git commit -m "A brief summary of the commit
    > 
    > A paragraph describing what changed and its impact."

## Coding conventions

Start reading our code and you'll get the hang of it. We optimize for readability:

Thanks,
JMColeman (Frozen Toad)
